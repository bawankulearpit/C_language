//To read a number and find sum of its digits.

#include<stdio.h>
int main(){

    int n;
    printf("Enter any digit number\n");
    scanf("%d",&n);

    int digit = 0;
    int sum = 0;

    while (n > 0){

        digit = n%10;
        sum = digit + sum;
        n = n/10;
}
    printf("sum is %d\n",sum);

}