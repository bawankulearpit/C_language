#include<stdio.h>
int main(){

    char color;
    printf("enter a color code (r,g,b)\n");
    scanf("%c",&color);

    switch(color){

        case 'r':
        case 'R':
            printf("RED");
            break;
        case 'g':
        case 'G':
            printf("GREEN");
            break;
        case 'b':
        case 'B':
            printf("BLUE");
            break;
        default:
            printf("invalid color");
    }

        printf("\n");
}