//  To print first 10 fibonacci number

#include<stdio.h>
int main(){

    int first = 0;
    int second = 1;
    int sum = first + second;

    for (int i = 0 ; i<=10 ; i++){

        sum = first + second;
        first = second;
        second = sum;

        printf("%d\n",second);
    }
}